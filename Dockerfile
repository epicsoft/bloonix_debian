FROM debian:9
LABEL image.name="bloonix_debian" \
      image.description="Docker container for the opensource monitoring software Bloonix, based on Debian image." \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2017 epicsoft.de / Alexander Schwarz" \
      license="MIT"

ENV BLOONIX_DB_HOST="postgres" \
    BLOONIX_DB_PORT="5432" \
    BLOONIX_DB_SCHEMA="bloonix" \
    BLOONIX_DB_USER="bloonix" \
    BLOONIX_DB_PASSWORD="bloonix" \
    BLOONIX_ES_HOST="elasticsearch" \
    BLOONIX_ES_PORT="9200" \
    BLOONIX_HOST_ID="1" \
    BLOONIX_HOST_PASSWORD="YourSecretAgentPasswordForHost" \
    BLOONIX_CHECKSUM="0bd450c4aa6267457790ad2aff2666e153f36abe495c6671db0d72a0470de5bf1373fb0796118b3ae15e45cf0748688c3ae1a71c810df72713e5fa734c94ce21" \
    GOREPLACE_VERSION="1.1.2" \
    GOREPLACE_CHECKSUM="8821544a2f1697664016bf49bd2da45b1d8838bb4ddf326fef795dc4a2735e4716e633bca1404ded10ea54d54fba478220f00e3b1e28e8e964787557803a8aea"

# system upgrade 
RUN apt-get -y update \
 && apt-get -y upgrade \
 # install runtime requirements
 && apt-get -y install apt-transport-https \
                       ca-certificates \ 
                       openjdk-8-jre \
                       postfix \
                       nginx \
 # cleanup apt
 && apt-get -y autoclean \
 && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/* \
 # workaround missing systemctl
 && touch /bin/systemctl \
 && chmod +x /bin/systemctl

# install installations requirements
RUN apt-get -y update \
 && apt-get -y install wget \
                       gnupg \
                       lsb-release \
 # install bloonix 
 && wget --timeout=30 --tries=10 --retry-connrefused -O /tmp/bloonix.gpg https://download.bloonix.de/repos/debian/bloonix.gpg \
 && sha512sum /tmp/bloonix.gpg \
 && echo $BLOONIX_CHECKSUM  /tmp/bloonix.gpg | sha512sum -c --strict \
 && apt-key add /tmp/bloonix.gpg \
 && echo "deb https://download.bloonix.de/repos/debian/ `lsb_release --codename | cut -f2` main" >> /etc/apt/sources.list.d/bloonix.list \
 && apt-get -y update \
 && apt-get -y install bloonix-webgui \
                       bloonix-server \
                       bloonix-agent \                       
                       bloonix-plugins-basic \
                       bloonix-plugins-linux \
 # install go-replace 
 && wget --timeout=30 --tries=10 --retry-connrefused -O /usr/local/bin/go-replace https://github.com/webdevops/go-replace/releases/download/$GOREPLACE_VERSION/gr-32-linux \
 && sha512sum /usr/local/bin/go-replace \
 && echo $GOREPLACE_CHECKSUM /usr/local/bin/go-replace | sha512sum -c --strict \
 && chmod +x /usr/local/bin/go-replace \
 # cleanup installations requirements and apt
 && apt-get -y purge wget \
                     gnupg \
                     lsb-release \
 && apt-get -y autoclean \
 && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/* \
 # move Bloonix original configuration
 && mv /etc/bloonix/database/main.conf /etc/bloonix/database/main.conf.orginal \
 && mv /etc/bloonix/agent/main.conf /etc/bloonix/agent/main.conf.orginal \
 && mv /etc/bloonix/webgui/main.conf /etc/bloonix/webgui/main.conf.orginal \
 && mv /etc/bloonix/server/main.conf /etc/bloonix/server/main.conf.orginal \
 && mv /etc/bloonix/srvchk/main.conf /etc/bloonix/srvchk/main.conf.orginal 

COPY ["./etc", "/etc"]

RUN chmod +x /etc/epicsoft/*.sh

VOLUME /etc/bloonix

EXPOSE 443 \
       5460

ENTRYPOINT ["/etc/epicsoft/entrypoint.sh"]

HEALTHCHECK --interval=60s --timeout=30s --start-period=20s --retries=3 CMD /etc/epicsoft/healtcheck.sh
